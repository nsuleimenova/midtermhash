package main

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

func ExecutePipeline(jobs ...job) {
	in := make(chan interface{})

	wg := sync.WaitGroup{}

	for _, task := range jobs {
		wg.Add(1)
		out := make(chan interface{})
		go func(task job, in, out chan interface{}, wg *sync.WaitGroup) {
			defer wg.Done()
			task(in, out)
		}(task, in, out, &wg)
		in = out
	}
	wg.Wait()

}

func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}

	for data := range in {
		wg.Add(1)
		go SingleHasher(data.(string), out, wg)
	}
	wg.Wait()
}

func SingleHasher(data string, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()

	var md5Data = DataSignerMd5(data)
	var crc32md5Data = DataSignerCrc32(md5Data)
	var crc32Data = DataSignerCrc32(data)

	result := crc32md5Data + "~" + crc32Data

	fmt.Println(data + " SingleHash data " + data)
	fmt.Println(data + " SingleHash md5(data) " + md5Data)
	fmt.Println(data + " SingleHash crc32(md5(data)) " + crc32md5Data)
	fmt.Println(data + " SingleHash crc32(data) " + crc32Data)
	fmt.Println(data + " SingleHash result " + result)

	out <- result

}
func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for data := range in {
		wg.Add(1)
		go MultiHasher(data, out, wg)
	}

	wg.Wait()
}

func MultiHasher(data interface{}, out chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()

	for th := 0; th < 6; th++ {
		wg.Add(1)
		go func(data interface{}, out chan interface{}, wg *sync.WaitGroup) {

		}(data, out, wg)
	}

	wg.Wait()
}

func CombineResults(in, out chan interface{}) {
	var MultiHashResult []string
	for data := range in {
		MultiHashResult = append(MultiHashResult, data.(string))
	}
	sort.Strings(MultiHashResult)
	finalResult := strings.Join(MultiHashResult, "_")
	fmt.Println("Combine Results " + finalResult)
	out <- finalResult

}
